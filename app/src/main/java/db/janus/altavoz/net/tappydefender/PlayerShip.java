package db.janus.altavoz.net.tappydefender;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

/**
 * Created by zlnk on 28-01-16.
 *
 * Esta clase contiene a la nave que manejaremos
 *
 */
public class PlayerShip {

    private final int GRAVITY = -12;

    //Para evitar que la nave abandone la pantalla
    private int maxY;
    private int minY;

    //Limites de velocidad para la nave
    private final int MIN_SPEED = 1;
    private final int MAX_SPEED = 20;

    /*  Aspectos minimos que se deben saber de la nave:
    *   - ¿Donde esta en la pantalla?
    *   - ¿Cómo se ve?
    *   - ¿A que velocidad se desplaza ? */

    private Bitmap bitmap;
    private int x,y;
    private int speed = 0;
    private boolean boosting;
    private int shieldStrength;

    /*  Para deteccion de colisiones    */
    private Rect hitBox;

    /*  Para cumplir con lo anterior, la nave debe ser capaz de :
    *   - Preparse
    *   - Actualizar su estado
    *   - Compartir el estado con la vista  */


    public PlayerShip(Context context, int screenX, int screenY){
        this.x = 50;
        this.y = 50;
        speed =1;
        bitmap = BitmapFactory.decodeResource(context.getResources(),R.drawable.ship);
        boosting = false;
        maxY = screenY - bitmap.getHeight();
        minY = 0;

        hitBox = new Rect(x,y,bitmap.getWidth(),bitmap.getHeight());
        shieldStrength = 2;

    }


    /*  Metodo que actualizara la posicion cuando sea llamado desde TDView  */
    public void update(){
        x ++;

        //Boost solo afectara a la altura
        if(boosting){
            speed += 2;
        }else {
            speed -= 5;

        }

        //Limites de velocidad
        if(speed > MAX_SPEED){
            speed = MAX_SPEED;
        }
        if(speed < MIN_SPEED){
            speed = MIN_SPEED;
        }

        // Acorde a la gravedad movemos la nave
        y -= speed + GRAVITY;

        //La nave no debe salir de la pantalla
        if( y < minY){
            y = minY;
        }
        if( y > maxY){
            y = maxY;
        }


        /*  Actualizar el HitBox que permitira establecer las coordenadas de seguridad de la nave*/
        hitBox.left = x;
        hitBox.top = y;
        hitBox.right = x + bitmap.getWidth();
        hitBox.bottom = y + bitmap.getHeight();

    }

    /*  Metodos para compartir su estado con la vista   */

    public Bitmap getBitmap(){
        return bitmap;
    }

    public int getSpeed(){
        return speed;
    }

    public int getX(){
        return x;
    }

    public int getY(){
        return y;
    }

    /*  Funciones para activar o desactivar la funcion BOOST*/
    public void setBoosting(){
        boosting = true;
    }

    public void stopBoosting(){
        boosting = false;
    }


    public Rect getHitBox(){
        return hitBox;
    }

    public int getShieldStrength(){
        return shieldStrength;
    }

    public void reduceShieldStrength(){
        shieldStrength --;
    }

}
