package db.janus.altavoz.net.tappydefender;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;

/**
 * Created by zlnk on 28-01-16.
 */
public class TDView extends SurfaceView implements Runnable {

    private boolean mIsPlaying;
    private Thread gameThread = null;
    private PlayerShip player;
    private EnemyShip enemyShip1;
    private EnemyShip enemyShip2;
    private EnemyShip enemyShip3;
    private ArrayList<SpaceDust> dustlist = new ArrayList<SpaceDust>();
    int numSpecs = 40;
    private Context mContext;

    /*  Elementos para el dibujado  */
    private Paint paint;
    private Canvas canvas;
    private SurfaceHolder ourHolder;


    /*  Variables de juego  */
    private float distanceRemaining;
    private long timeTaken;
    private long timeStarted;
    private long fastestTime;
    private int screenX;
    private int screenY;

    /*  Condicion para terminar el juego    */
    private boolean gameEnded;

            /*  Constructores   */
    public TDView(Context context,int x, int y) {
        super(context);
        mContext = context;
        screenX = x;
        screenY = y;
        startGame();
    }

    public TDView(Context context, AttributeSet attrs,int x, int y) {
        super(context, attrs);
        mContext = context;
        screenX = x;
        screenY = y;
        startGame();
    }

    public TDView(Context context, AttributeSet attrs, int defStyleAttr,int x, int y) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        screenX = x;
        screenY = y;
        startGame();
    }



    private void InitializeDrawingObjects(Context context, int x, int y){
        mContext = context;
        ourHolder = getHolder();
        paint = new Paint();
        player = new PlayerShip(context,x,y);
        enemyShip1 = new EnemyShip(context,x,y);
        enemyShip2 = new EnemyShip(context,x,y);
        enemyShip3 = new EnemyShip(context,x,y);

        /*  Polvo estelar   */
        for (int i = 0; i < numSpecs; i++){
            SpaceDust spec = new SpaceDust(x,y);
            dustlist.add(spec);
        }

    }


    private void startGame(){

        InitializeDrawingObjects(mContext,screenX,screenY);
        /*  Inicializamos las variables de distancia y tiempo   */
        distanceRemaining = 10000;
        timeTaken = 0;
        timeStarted = System.currentTimeMillis();
        gameEnded = false;
    }


            /*  Parte del Runnable  */
    @Override
    public void run() {
        while (mIsPlaying){
            update();
            draw();
            control();
        }
    }

    /*  Por cada frame, o unidad de tiempo que transcurra en el juego, se actualizan los objetos con los nuevos datos*/
    private void update(){
        player.update();
        enemyShip1.update(player.getSpeed());
        enemyShip2.update(player.getSpeed());
        enemyShip3.update(player.getSpeed());

        for(SpaceDust sd : dustlist){
            sd.update(player.getSpeed());
        }
        boolean hitdetected = false;
        /*Deteccion de colisiones de nuestra nave con los enemigos  */
        if(Rect.intersects(player.getHitBox(),enemyShip1.getHitBox())){
            hitdetected = true;
            enemyShip1.setX(-100);
        }
        if(Rect.intersects(player.getHitBox(),enemyShip2.getHitBox())){
            hitdetected = true;
            enemyShip2.setX(-100);
        }
        if(Rect.intersects(player.getHitBox(),enemyShip3.getHitBox())){
            hitdetected = true;
            enemyShip3.setX(-100);
        }

        /*  Acciones para cuando la nave es golpeada    */
        if(hitdetected){
            player.reduceShieldStrength();
            if(player.getShieldStrength()< 0){
                gameEnded = true;
            }
        }

        if(!gameEnded){
            /*  Actualizamos los valores a mostrar por pantalla */
            distanceRemaining -= player.getSpeed();

            /*  Tiempo de vuelo   */
            timeTaken = System.currentTimeMillis() - timeStarted;
        }

        /*  Acciones para cuando se completa el juego   */
        if(distanceRemaining < 0){

            /*  Verificamos el tiempo mas rapido    */
            if(timeTaken < fastestTime){
                fastestTime = timeTaken;
            }
            distanceRemaining = 0;
            gameEnded = true;
        }

    }


    /*  Una vez actualizados los datos de los objetos, se redibujara la imagen, */
    private void draw(){

        if(ourHolder.getSurface().isValid()){
            //Bloquearemos el sector de memoria en donde realizaremos el dibujado
            canvas = ourHolder.lockCanvas();

            //Eliminamos el ultimo frame
            canvas.drawColor(Color.argb(255,0,0,0));

            //Dibujamos al jugador
            canvas.drawBitmap(
                    player.getBitmap(),
                    player.getX(),
                    player.getY(),
                    paint);

            /*Dibujamos las naves enemigas  */
            canvas.drawBitmap(
                    enemyShip1.getBitmap(),
                    enemyShip1.getX(),
                    enemyShip1.getY(),
                    paint
            );

            canvas.drawBitmap(
                    enemyShip2.getBitmap(),
                    enemyShip2.getX(),
                    enemyShip2.getY(),
                    paint
            );

            canvas.drawBitmap(
                    enemyShip3.getBitmap(),
                    enemyShip3.getX(),
                    enemyShip3.getY(),
                    paint
            );


            /*  Pintamos el polvo estelar   */
            paint.setColor(Color.argb(255, 255, 255, 255));

            for(SpaceDust sd : dustlist){
                canvas.drawPoint(sd.getX(),sd.getY(),paint);
            }

            /*  Marcamos los hitbox  -- debugging */
//            canvas.drawRect(player.getHitBox().left,
//                    player.getHitBox().top,
//                    player.getHitBox().right,
//                    player.getHitBox().bottom,paint);


            //Dibujamos elementos del HUD
            if(!gameEnded) {
                paint.setTextAlign(Paint.Align.LEFT);
                paint.setColor(Color.argb(255, 255, 255, 255));
                paint.setTextSize(25);
                canvas.drawText("Tiempo mas rapido: " + fastestTime + "s", 10, 20, paint);
                canvas.drawText("Tiempo: " + timeTaken + "s", screenX / 2, 20, paint);
                canvas.drawText("Distancia: " + distanceRemaining / 1000 + "KM", screenX / 3, screenY - 20, paint);
                canvas.drawText("Escudo: " + player.getShieldStrength(), 10, screenY - 20, paint);
                canvas.drawText("Velocidad " + player.getSpeed() * 60 + "Kms.", (screenX / 3) * 2, screenY - 20, paint);
            }else {
                    paint.setTextSize(80);
                    paint.setTextAlign(Paint.Align.CENTER);
                    canvas.drawText("Has llegado a casa",screenX/2,100,paint);
                    canvas.drawText("Tiempo mas rapido: " + fastestTime + "s", 10, 20, paint);
                    canvas.drawText("Tiempo: " + timeTaken + "s", screenX / 2, 20, paint);
                    canvas.drawText("Distancia: " + distanceRemaining / 1000 + "KM", screenX / 3, screenY - 20, paint);

            }

            //Desbloqueamos el canvas y dibujamos la escena
            ourHolder.unlockCanvasAndPost(canvas);


        }


    }

    /*  Control permitira determinar la frecuencia con que los dos metodos anteriores suceden   */
    private void control(){

        /*Estimaremos que la tasa de refresco sea de 60FPS 1000/60 = 17*/
        try {
            gameThread.sleep(17);
        }catch (InterruptedException e){

        }

    }

    /*Se implementara un metodo, para cuando el usuario abandone el juego*/
    public void pause(){
        mIsPlaying = false;
        try {
            gameThread.join();
        }catch (InterruptedException e){

        }
    }

    /*  Creamos un nuevo thread, y reiniciamos el juego */
    public void resume(){
        mIsPlaying = true;
        gameThread = new Thread(this);
        gameThread.start();
    }



    /*  Acciones para capturar las ordenes del jugador  */

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        /*  De momento solo nos preocuparemos de dos acciones, subir y bajar    */
        switch (event.getAction() & MotionEvent.ACTION_MASK){
            case MotionEvent.ACTION_UP:
                //Cuando se arrastra hacia arriba
                player.setBoosting();
                if(gameEnded){
                    startGame();
                }

                break;
            case MotionEvent.ACTION_DOWN:
            //Cuando se arrastre hacia abajo
                player.stopBoosting();
                break;
        }
        return true;
    }



}
