package db.janus.altavoz.net.tappydefender;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        final Button buttonPlay =
                (Button)findViewById(R.id.buttonPlay);
        buttonPlay.setOnClickListener(this);

    }

        /*  Llevara a cabo las acciones, cuando se presione el boton play*/
    @Override
    public void onClick(View v) {

        /*  Creamos un objeto del tipo intent, para dar inicio al juego */
        Intent i = new Intent(this,GameActivity.class);
        /*  Comenzamos la actividad a través del intent */
        startActivity(i);
        /*  Cerramos la actividad actual    */
        finish();
    }
}
