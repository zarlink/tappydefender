package db.janus.altavoz.net.tappydefender;

import java.util.Random;

/**
 * Created by zlnk on 29-01-16.
 */
public class SpaceDust {

    /*  Variables de ubicacion y desplazamiento*/
    private  int x,y;
    private int speed;

    /*  Para detectar cuando el polvo deja la pantalla  */
    private int maxX;
    private int maxY;
    private int minX;
    private int minY;


    /*  Constructor */
    public SpaceDust(int screenX, int screenY){
        maxX = screenX;
        maxY = screenY;
        minX = 0;
        minY = 0;


        /*  Fijaremos una velocidad entre 0 y 9 */
        Random generator =  new Random();
        speed = generator.nextInt(10);

        /*  Coordenadas iniciales para comenzar */
        x = generator.nextInt(maxX);
        y = generator.nextInt(maxY);


    }

    /*  Actualizamos la posicion    */

    public void update(int playerSpeed){
        x -= playerSpeed;
        x -= speed;


        /*  Reiniciamos las estrellas cuando estas se encuentran fuera de la pantalla   */
        if(x < 0){
            x = maxX;
            Random generator = new Random();
            y = generator.nextInt(maxY);
            speed = generator.nextInt(15);
        }

    }


    /*  Funciones para comentar el estado del polvo estelar con la parte visual */
    public  int getX(){
        return x;
    }
    public int getY(){
        return y;
    }

}
