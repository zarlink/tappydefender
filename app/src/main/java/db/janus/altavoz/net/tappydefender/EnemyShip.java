package db.janus.altavoz.net.tappydefender;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

import java.util.Random;

/**
 * Created by zlnk on 29-01-16.
 */
public class EnemyShip {

    /*  Variables minimas para determinar a la nave   */
    private Bitmap bitmap;
    private int x,y;
    private int speed = 1;


    /*  Variables para detectar cuando el enemigo abandone la pantalla  */
    private int maxX;
    private int minX;

    /*  Instanciar a los enemigos dentro del margen de la pantalla*/
    private int maxY;
    private int minY;

    /*  Para deteccion de colisiones    */
    private Rect hitBox;


    /*  Constructor */
    public EnemyShip(Context context, int screenX, int screenY){
        bitmap = BitmapFactory.decodeResource(context.getResources(),R.drawable.enemy);
        maxX = screenX;
        maxY = screenY;
        minX = 0;
        minY = 0;

        Random generator = new Random();
        speed = generator.nextInt(6)+10;

        x = screenX;
        y = generator.nextInt(maxY) - bitmap.getHeight();

        hitBox = new Rect(x,y,bitmap.getWidth(),bitmap.getHeight());


    }


    /*  Al igual que PlayerShip, dispondrá de métodos minimos para notificarle a la vista de su estado. */

    public Bitmap getBitmap(){
        return bitmap;
    }

    public int getX(){
        return x;
    }

    public int getY(){
        return y;
    }

    /*  Update determinará la velocidad de la nave enemiga en base a la velocidad del jugador*/

    public void update(int playerSpeed){

        //Movimiento a la izquierda de la pantalla
        x -= playerSpeed;
        x -= speed;

        //Reiniciaremos la nave cuando salga de la pantalla
        if (x < minX -bitmap.getWidth()){
            Random generator = new Random();
            speed = generator.nextInt(10) +10;
            x = maxX;
            y = generator.nextInt(maxY) - bitmap.getHeight();
        }

                /*  Actualizar el HitBox que permitira establecer las coordenadas de seguridad de la nave*/
        hitBox.left = x;
        hitBox.top = y;
        hitBox.right = x + bitmap.getWidth();
        hitBox.bottom = y + bitmap.getHeight();


    }


    public Rect getHitBox(){
        return hitBox;
    }

    /*  Con este metodo reiniciaremos la nave, cuando esta choque   */

    public void setX(int x){
        this.x = x;
    }

}
