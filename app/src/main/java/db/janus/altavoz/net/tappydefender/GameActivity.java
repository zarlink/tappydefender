package db.janus.altavoz.net.tappydefender;

import android.app.Activity;
import android.graphics.Point;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;

public class GameActivity extends Activity {
    private TDView gameView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_game);

        /*Capturaremos las dimensiones de la pantalla   */
        Display display = getWindowManager().getDefaultDisplay();
        //Cargamos la resolucion en un objeto del tipo punto
        Point size = new Point();
        display.getSize(size);
        gameView = new TDView(this,size.x,size.y);
        setContentView(gameView);

    }

    /*  Si se pausa la actividad, pausaremos el thread que mantiene la logica del juego*/
    @Override
    protected void onPause() {
        super.onPause();
        gameView.pause();
    }

    /*  Idem para cuando se resuma la actividad, nos encargaremos que el thread se encuentre en las mismas condiciones*/
    @Override
    protected void onResume() {
        super.onResume();
        gameView.resume();
    }
}
